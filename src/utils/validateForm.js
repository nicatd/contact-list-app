export const validateForm = (fields, e) => {
  if (fields.name.trim() === "") {
    alert("Name is required, please, fill the gap");
    e.preventDefault();
    return false;
  } else if (fields.tel.trim() === "") {
    alert("Telephone number is required, please, write your telephone number");
    e.preventDefault();
    return false;
  } else if (fields.email.trim() === "") {
    alert("Email is required, please, write your email address");
    e.preventDefault();
    return false;
  } else if (fields.address.trim() === "") {
    alert("Address is required, please, write your address");
    e.preventDefault();
    return false;
  } else return true;
};
